//Simulate the '$' form jQuery
const $ = document.querySelector.bind(document);

//Menu Opener
const menuIsOpenClass = 'menu-is-open';
const menuOpener = $('#menu-opener');
const menuCloser = $('#menu-closer');
const body = $('body');

function toggleMenu(state) {
    body.classList[state ? 'add' : 'remove'](menuIsOpenClass);
}

menuOpener.addEventListener('click', event => toggleMenu(true));
menuCloser.addEventListener('click', event => toggleMenu(false));