# Teste de Front-End para a Editora Sanar

Tecnologias Utilizadas

* Gulp
* JavaScript
* HTML5
* CSS3
* SASS
* Weskit (https://github.com/HDeiro/weskit)
* Efecss (https://github.com/efecss)

Para executar o projeto, basta visualizar os arquivos gerados pelo Gulp em WWW. Caso deseje fazer alguma alteração, basta executar o comando ```npm i``` ou ```npm install``` para baixar todas as libs e em seguida executar ```gulp css js views  images server```, que subirá um servidor de desenvolvimento que já estará observando os arquivos para atualizá-los em cada edição.

Ah, observação. Como utilizei o template Weskit e ele ainda não tratou fontes na pasta de desenvolvimento, talvez seja necessário copiar a pasta fontes para lá.